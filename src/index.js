const express = require('express');
const path = require('path');
const exphbs = require('express-handlebars');
const methodOverride = require('method-override');
const session = require('express-session');
const morgan = require('morgan');


//The best
const chalk = require('chalk');

//Initilizations
const app = express();


// Settings 
app.set('port',process.env.PORT || 3000);
app.set('views', path.join(__dirname,'views'));
app.engine('.hbs', exphbs({
	defaultLayout:'main',
	layoutsDir:path.join(app.get('views'),'layaouts'),
	partialsDir:path.join(app.get('views'),'partials'),
	extname: '.hbs'
}));
app.set('view engine', '.hbs');
//Middlewares
app.use(express.urlencoded({
	extended:false
}));
app.use(methodOverride('_method'));
app.use(session({
	secret:'mysecretApp',
	resave:true,
	saveUninitialized:true
}));
app.use(morgan('dev'));
//Global Variables

//Routes
app.use(require('./routes/index'));
// app.use(require('./routes/users'));
// app.use(require('./routes/task'));


//Static Files

//Server is Listening

app.listen(
	app.get('port'), () =>{
		console.log(chalk.bgGreen.black(
			`Server listen on ${app.get('port')}`
			));
	}
)